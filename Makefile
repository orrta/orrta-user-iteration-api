help:
	@echo 'Makefile for orrta_user_iteration_api                                                  '
	@echo '                                                                             '
	@echo 'Usage:                                                                       '
	@echo '   install                                 Install all dependencies to run   '
	@echo '   setup                                   Install all dependencies to dev   '
	@echo '   run_local                               Run project using local enviroment'

install:
	@pip install -r requirements.txt

setup:
	@pip install -r requirements_dev.txt

test_code_quality:
	@echo "========================== TEST CODE QUALITY =========================="
	@py.test orrta_user_iteration_api --pep8 --flakes --mccabe

run_local:
	NEW_RELIC_ENABLED="False" \
	PYTHONUNBUFFERED=1 \
	PORT=8283 \
	python web.py

clean:
	@find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
