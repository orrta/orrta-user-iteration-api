# orrta-user-iteration-api

[![Python Version](https://img.shields.io/badge/python-3.6-green.svg)](https://img.shields.io/badge/python-3.6-green.svg)
[![pipeline status](https://gitlab.com/orrta/orrta-aggregation-api/badges/master/pipeline.svg)](https://gitlab.com/orrta/orrta-aggregation-api/commits/mas    ter)

## Requirements
  - Python 3.6
  - Docker

## Get help
  - ``` make help ```

## How to setup
- ``` git clone https://gitlab.com/orrta/orrta-user-iteration-api.git ```
- ``` cd orrta-user-iteration-api ```
- ``` make setup ```

## How to run
- ``` make run_local ```
