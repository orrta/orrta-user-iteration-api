from measures import Measure
from orrta_user_iteration_api import settings
import logging
import redis

logger = logging.getLogger('redis')
redis_connection = redis.Redis.from_url(settings.REDIS_URL)

measure = Measure('user-iteration-api-cache', (settings.MEASURES_URL, settings.MEASURES_PORT))


class CacheProvider:

    def delete(self, keys):
        try:
            for key in keys:
                self.delete_one(key)
        except Exception:
            logger.exception('failed to delete caches')

    def delete_one(self, key):
        try:
            redis_connection.delete(key)
            measure.count('clean', dimensions={'key': str(key)})
        except Exception:
            logger.exception('failed to delete cache')
            measure.count('clean-error', dimensions={'key': str(key)})

    def keys(self, pattern):
        try:
            cache_keys = redis_connection.keys(pattern)
            return cache_keys or []
        except Exception:
            logger.exception('failed to get cache keys')
            return []
