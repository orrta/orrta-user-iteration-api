from orrta_user_iteration_api import settings
from pyfcm import FCMNotification
import logging


class FcmNotifier(object):

    def __init__(self):
        self._logger = logging.getLogger('FcmNotifier')
        self._push_service = FCMNotification(api_key=settings.FIREBASE_API_KEY)

    def notify(self, registration_id, title, body):
        try:
            self._logger.info('Notifying {id}: title: {title}; body: {body}'.format(
                id=registration_id,
                title=title,
                body=body
            ))

            self._push_service.notify_single_device(
                registration_id=registration_id,
                message_title=title,
                message_body=body
            )

            self._logger.info('Success to notify {id}: title: {title}; body: {body}'.format(
                id=registration_id,
                title=title,
                body=body
            ))
        except Exception:
            self._logger.exception('Failed to notify {id}: title: {title}; body: {body}'.format(
                id=registration_id,
                title=title,
                body=body
            ))
