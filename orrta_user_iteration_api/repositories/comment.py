from orrta_user_iteration_api import settings
from orrta_user_iteration_api.repositories import RepositoryException
from orrta_user_iteration_api.components.cache import CacheProvider
from tornado.httpclient import AsyncHTTPClient
import datetime
import logging
import json

logger = logging.getLogger('CommentRepository')
cache_provider = CacheProvider()


class CommentRepository():

    def __init__(self):
        self.http_client = AsyncHTTPClient()

    async def create(self, comment):
        comment['date_time'] = str(datetime.datetime.now().isoformat())

        url = '{url}/comments/{id}'.format(
            id=comment['id'],
            url=settings.AGGREGATION_API_URL
        )

        response = await self.http_client.fetch(
            url,
            raise_error=False,
            method='POST',
            body=json.dumps(comment),
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        if 200 <= response.code < 300:
            logger.info('Comment saved with success')

            keys = cache_provider.keys('comments-default--{post_id}-*'.format(
                post_id=comment['postId']
            ))

            cache_provider.delete(keys)
        else:
            error = (
                'Failed to save comment. '
                'status code: {status_code}. '
                'error: {error}.'.format(
                    status_code=response.code,
                    error=response.body
                )
            )

            logger.error(error)
            raise RepositoryException(error)

    async def delete(self, id, post_id):
        url = '{url}/comments/{id}'.format(
            id=id,
            url=settings.AGGREGATION_API_URL
        )

        response = await self.http_client.fetch(
            url,
            raise_error=False,
            method='DELETE',
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        if 200 <= response.code < 300:
            logger.info('Comment deleted with success')

            keys = cache_provider.keys('comments-default--{post_id}--*'.format(
                post_id=post_id
            ))

            cache_provider.delete(keys)
        else:
            error = (
                'Failed to delete comment. '
                'status code: {status_code}. '
                'error: {error}.'.format(
                    status_code=response.code,
                    error=response.body
                )
            )

            logger.error(error)
            raise RepositoryException(error)
