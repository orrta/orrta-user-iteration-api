from orrta_user_iteration_api import settings
from orrta_user_iteration_api.components.cache import CacheProvider
from orrta_user_iteration_api.repositories import RepositoryException
from tornado.httpclient import AsyncHTTPClient
from urllib.parse import quote_plus
import tornado
import json
import logging

logger = logging.getLogger('DiscountCouponRepository')
cache_provider = CacheProvider()


class DiscountCouponRepository():

    def __init__(self):
        self.http_client = AsyncHTTPClient()

    async def save(self, discount_coupon):
        doc_id = '{user_id}:{code}'.format(
            user_id=quote_plus(discount_coupon['user_id']),
            code=quote_plus(discount_coupon['code']),
        )

        url = '{url}/discount-coupon/{id}'.format(
            id=doc_id,
            url=settings.AGGREGATION_API_URL
        )

        response = await self.http_client.fetch(
            url,
            raise_error=False,
            method='POST',
            body=json.dumps(discount_coupon),
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        if 200 <= response.code < 300:
            logger.info('Discount coupon saved with success')

            cache_key = '{index}:{id}'.format(
                index='discount-coupon',
                id=doc_id
            )

            cache_provider.delete_one(cache_key)

            cache_key = '{index}:{user_id}:{code}'.format(
                index='discount-coupon',
                user_id=discount_coupon['user_id'],
                code=discount_coupon['code'],
            )

            cache_provider.delete_one(cache_key)
        else:
            error = (
                'Failed to save discount coupon. '
                'status code: {status_code}. '
                'error: {error}.'.format(
                    status_code=response.code,
                    error=response.body
                )
            )
            logger.error(error)
            raise RepositoryException(error)

    async def code_is_already_used(self, user_id, code):
        url = '{url}/discount-coupon/{user_id}:{code}'.format(
            url=settings.AGGREGATION_API_URL,
            user_id=quote_plus(user_id),
            code=quote_plus(code)
        )

        try:
            response = await self.http_client.fetch(
                url,
                method='GET',
                headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
            )

            body = json.loads(response.body)
            return body['es']['found']

        except tornado.httpclient.HTTPError as e:
            if e.response.code == 404:
                return False

            raise e
