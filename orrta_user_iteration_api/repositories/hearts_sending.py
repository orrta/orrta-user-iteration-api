from orrta_user_iteration_api import settings
from orrta_user_iteration_api.components.cache import CacheProvider
from orrta_user_iteration_api.repositories import RepositoryException
from tornado.httpclient import AsyncHTTPClient
import logging
import json

logger = logging.getLogger('DiscountCouponRepository')
cache_provider = CacheProvider()


class HeartsSendingRepository():

    def __init__(self):
        self.http_client = AsyncHTTPClient()

    async def save(self, hearts_sending):
        post_id = hearts_sending['post_id']

        id = '{user_id}:{post_id}'.format(
            user_id=hearts_sending['user_id'],
            post_id=post_id
        )

        url = '{url}/hearts-sending/{id}'.format(id=id, url=settings.AGGREGATION_API_URL)

        if hearts_sending['count'] == 0:
            hearts_sending['count'] = 1

        existent_hearts_sending = await self.get_hearts_sending(id)

        if existent_hearts_sending:
            hearts_sending['count'] = hearts_sending['count'] + existent_hearts_sending['count']

            hearts_sending = {'doc': hearts_sending}
            url = '{url}/hearts-sending/{id}/update'.format(id=id, url=settings.AGGREGATION_API_URL)
        else:
            url = '{url}/hearts-sending/{id}'.format(id=id, url=settings.AGGREGATION_API_URL)

        response = await self.http_client.fetch(
            url,
            method='POST',
            raise_error=False,
            body=json.dumps(hearts_sending),
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        if 200 <= response.code < 300:
            logger.info('Hearts sending saved with success')

            cache_key = '{index}:{id}'.format(
                index='hearts-sending',
                id=id
            )

            keys = cache_provider.keys('ranking-default--{post_id}-*'.format(
                post_id=post_id
            )) + [cache_key]

            cache_provider.delete(keys)
        else:
            error = (
                'Failed to save hearts sending. '
                'status code: {status_code}. '
                'error: {error}.'.format(
                    status_code=response.code,
                    error=response.body
                )
            )
            logger.error(error)
            raise RepositoryException(error)

    async def get_hearts_sending(self, id):
        url = '{url}/hearts-sending/{id}'.format(id=id, url=settings.AGGREGATION_API_URL)

        response = await self.http_client.fetch(
            url,
            raise_error=False,
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        if 200 <= response.code < 300:
            return json.loads(response.body)['es'].get('_source', False)

        return False
