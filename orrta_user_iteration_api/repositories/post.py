from orrta_user_iteration_api import settings
from orrta_user_iteration_api.components.cache import CacheProvider
from orrta_user_iteration_api.repositories import RepositoryException
from tornado.httpclient import AsyncHTTPClient
import logging
import json

logger = logging.getLogger('PostRepository')
cache_provider = CacheProvider()


class PostRepository():

    def __init__(self):
        self.http_client = AsyncHTTPClient()

    async def update_comments_count(self, post_id, post_section, decrement=False):
        logger.info('Saving comments count')

        url = '{url}/{index}/{id}/update'.format(
            id=post_id,
            index=post_section,
            url=settings.AGGREGATION_API_URL
        )

        if decrement:
            script_update_body = {
                "script": {
                    "source": "ctx._source.comments_count -= 1",
                    "lang": "painless"
                }
            }
        else:
            script_update_body = {
                "script": {
                    "source": "if (ctx._source.containsKey(\"comments_count\")) { ctx._source.comments_count += 1 } else { ctx._source.comments_count = 1 }",
                    "lang": "painless"
                }
            }

        response = await self.http_client.fetch(
            url,
            raise_error=False,
            method='POST',
            body=json.dumps(script_update_body),
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        if 200 <= response.code < 300:
            logger.info('Comments count saved with success')

            keys = cache_provider.keys('timeline-*')
            cache_provider.delete(keys)
        else:
            error = (
                'Failed to save comments count. '
                'status code: {code}. '
                'error: {error}.'.format(
                    code=response.code,
                    error=response.body
                )
            )

            logger.error(error)
            raise RepositoryException(error)

    async def update_hearts(self, post_id, section, count):
        url = '{url}/{index}/{id}/update'.format(
            id=post_id,
            index=section,
            url=settings.AGGREGATION_API_URL
        )

        if count == 0:
            count = 1

        script_update_body = {
            "script": {
                "source": """
                    if (ctx._source.containsKey(\"hearts_count\")) {
                        ctx._source.hearts_count += params.count
                    } else {
                        ctx._source.hearts_count = params.count
                    }
                """,
                "lang": "painless",
                "params": {"count": count}
            }
        }

        response = await self.http_client.fetch(
            url,
            method='POST',
            body=json.dumps(script_update_body),
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        if 200 <= response.code < 300:
            logger.info('Hearts count saved with success')

            keys = cache_provider.keys('timeline-*')
            cache_provider.delete(keys)
        else:
            error = (
                'Failed to save hearts count. '
                'status code: {code}. '
                'error: {error}.'.format(
                    code=response.code,
                    error=response.body
                )
            )
            logger.error(error)
            raise RepositoryException(error)
