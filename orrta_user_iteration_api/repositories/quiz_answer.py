from orrta_user_iteration_api import settings
from orrta_user_iteration_api.components.cache import CacheProvider
from orrta_user_iteration_api.repositories import RepositoryException
from tornado.httpclient import AsyncHTTPClient
import logging
import json


logger = logging.getLogger('QuizAwnserRepository')
cache_provider = CacheProvider()


class QuizAwnserRepository():

    def __init__(self):
        self.http_client = AsyncHTTPClient()

    async def save(self, awnser):
        id = '{post_id}:{user_id}'.format(
            user_id=awnser['userId'],
            post_id=awnser['postId']
        )

        url = '{url}/quiz-answer/{id}'.format(id=id, url=settings.AGGREGATION_API_URL)

        response = await self.http_client.fetch(
            url,
            raise_error=False,
            method='POST',
            body=json.dumps(awnser),
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        if 200 <= response.code < 300:
            logger.info('Quiz answer saved with success')

            cache_provider.delete_one('{index}:{id}'.format(
                index='quiz-answer',
                id=id
            ))
        else:
            error = (
                'Failed to save quiz answer. '
                'status code: {status_code}. '
                'error: {error}.'.format(
                    status_code=response.code,
                    error=response.body
                )
            )
            logger.error(error)
            raise RepositoryException(error)
