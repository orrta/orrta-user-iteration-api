from orrta_user_iteration_api import settings
from orrta_user_iteration_api.repositories import RepositoryException
from tornado.httpclient import AsyncHTTPClient
import datetime
import logging
import json

logger = logging.getLogger('SubscriptionRepository')


class SubscriptionRepository():

    def __init__(self):
        self.http_client = AsyncHTTPClient()

    async def save_transaction(self, subscription_transaction):
        subscription_transaction['date_time'] = str(datetime.datetime.now().isoformat())

        url = '{url}/subscription-transactions'.format(url=settings.AGGREGATION_API_URL)

        response = await self.http_client.fetch(
            url,
            raise_error=False,
            method='POST',
            body=json.dumps(subscription_transaction),
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        if 200 <= response.code < 300:
            logger.info('Subscription Transaction saved with success')
        else:
            error = (
                'Failed to save Subscription Transaction. '
                'status code: {status_code}. '
                'error: {error}.'.format(
                    status_code=response.code,
                    error=response.body
                )
            )
            logger.error(error)
            raise RepositoryException(error)
