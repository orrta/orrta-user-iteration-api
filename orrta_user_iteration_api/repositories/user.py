from orrta_user_iteration_api import settings
from orrta_user_iteration_api.components.cache import CacheProvider
from orrta_user_iteration_api.repositories import RepositoryException
from tornado.httpclient import AsyncHTTPClient
import json
import logging
import datetime

logger = logging.getLogger('UserRepository')
cache_provider = CacheProvider()


class UserRepository():

    def __init__(self):
        self.http_client = AsyncHTTPClient()

    async def create_or_update(self, user):
        user_id = user['id']

        there_is_user = await self.is_there_user(user_id)

        if there_is_user:
            user = {'doc': user}
            url = '{url}/user/{id}/update'.format(id=user_id, url=settings.AGGREGATION_API_URL)
        else:
            user['available_hearts'] = 100
            user['sent_hearts'] = 0
            user['start_trial'] = str(datetime.datetime.now().isoformat())
            url = '{url}/user/{id}'.format(id=user_id, url=settings.AGGREGATION_API_URL)

        response = await self.http_client.fetch(
            url,
            raise_error=False,
            method='POST',
            body=json.dumps(user),
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        if 200 <= response.code < 300:
            logger.info('User saved with success')
            self._delete_user_from_cache(user_id)
            self.delete_comments_from_cache()
        else:
            error = (
                'Failed to save user. '
                'status code: {status_code}. '
                'error: {error}.'.format(
                    status_code=response.code,
                    error=response.body
                )
            )

            logger.error(error)
            raise RepositoryException(error)

    async def is_there_user(self, id):
        url = '{url}/user/{id}'.format(id=id, url=settings.AGGREGATION_API_URL)

        response = await self.http_client.fetch(
            url,
            raise_error=False,
            method='GET',
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        return 200 <= response.code < 300

    async def get_owner_by_sharing_code(self, code):
        logger.info('Getting owner of code {code}'.format(code=code))

        url = '{url}/user/_search'.format(url=settings.AGGREGATION_API_URL)

        response = await self.http_client.fetch(
            url,
            raise_error=False,
            body=json.dumps({"query": {"term": {"sharingCode.keyword": {"value": code}}}}),
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        if 200 <= response.code < 300:
            logger.info('Owner found')
            body = json.loads(response.body)
            return body['es']['hits']['total'] > 0 and body['es']['hits']['hits'][0]['_source']
        else:
            logger.error(
                'Failed getting owner code. '
                'status code: {status_code}. '
                'error: {error}.'.format(
                    status_code=response.code,
                    error=response.body
                )
            )
            return False

    async def update_hearts(self, user_id, count, discount_available_hearts=False):
        url = '{url}/user/{id}/update'.format(id=user_id, url=settings.AGGREGATION_API_URL)

        if discount_available_hearts:
            script_update_body = {
                "script": {
                    "source":
                        "ctx._source.available_hearts -= params.available_hearts_count;"
                        "ctx._source.sent_hearts += params.sent_hearts_count",
                    "lang": "painless",
                    "params": {
                        "available_hearts_count": count,
                        "sent_hearts_count": count or 1
                    }
                }
            }
        else:
            script_update_body = {
                "script": {
                    "source":
                        "ctx._source.available_hearts += params.count;",
                    "lang": "painless",
                    "params": {
                        "count": count
                    }
                }
            }

        response = await self.http_client.fetch(
            url,
            raise_error=False,
            method='POST',
            body=json.dumps(script_update_body),
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        if 200 <= response.code < 300:
            logger.info('User hearts saved with success')
            self._delete_user_from_cache(user_id)
        else:
            error = (
                'Failed to save user available hearts. '
                'status code: {status_code}. '
                'error: {error}.'.format(
                    status_code=response.code,
                    error=response.body
                )
            )
            logger.error(error)
            raise RepositoryException(error)

    async def set_as_subscriber(self, user_id, subscription):
        url = '{url}/user/{id}/update'.format(id=user_id, url=settings.AGGREGATION_API_URL)

        response = await self.http_client.fetch(
            url,
            method='POST',
            raise_error=False,
            body=json.dumps({'doc': {'subscriber': True, 'subscription': subscription}}),
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        if 200 <= response.code < 300:
            logger.info('User setted as subscriber with success')
            self._delete_user_from_cache(user_id)
        else:
            error = (
                'Failed to set user as subscriber. '
                'status code: {status_code}. '
                'error: {error}.'.format(
                    status_code=response.code,
                    error=response.body
                )
            )
            logger.error(error)
            raise RepositoryException(error)

    async def get_by_sharing_code(self, sharing_code):
        url = '{url}/user/_search'.format(
            url=settings.AGGREGATION_API_URL
        )

        response = await self.http_client.fetch(
            url,
            method='POST',
            body=json.dumps({
                "query": {"term": {"sharingCode.keyword": {"value": sharing_code}}}
            }),
            headers={
                'Authorization': settings.AGGREGATION_API_AUTH_KEY
            }
        )

        body = json.loads(response.body)

        if body['es']['hits']['total'] == 1:
            return body['es']['hits']['hits'][0]['_source']

    async def get_fcm_token(self, user_id):
        logger.info('Getting owner with id {id}'.format(id=user_id))

        url = '{url}/user/{id}'.format(id=user_id, url=settings.AGGREGATION_API_URL)

        response = await self.http_client.fetch(
            url,
            raise_error=False,
            method='GET',
            headers={'Authorization': settings.AGGREGATION_API_AUTH_KEY}
        )

        if 200 <= response.code < 300:
            logger.info('User found')
            body = json.loads(response.body)
            return body['es']['_source']['fcmToken']
        else:
            error = (
                'Failed getting owner code. '
                'status code: {status_code}. '
                'error: {error}.'.format(
                    status_code=response.code,
                    error=response.body
                )
            )
            logger.error(error)
            raise RepositoryException(error)

    def _delete_user_from_cache(self, user_id):
        cache_provider.delete_one('{index}:{id}'.format(
            index='user',
            id=user_id
        ))

    def delete_comments_from_cache(self):
        keys = cache_provider.keys('comments-default--*')
        cache_provider.delete(keys)
