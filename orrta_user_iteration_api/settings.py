import os

settings_dir = os.path.join(os.path.dirname(__file__), 'settings')

MEASURES_URL = os.environ.get(
    'MEASURES_URL',
    'ec2-18-216-60-243.us-east-2.compute.amazonaws.com'
)

MEASURES_PORT = os.environ.get('MEASURES_PORT', 1984)

AUTH_KEY = os.environ.get('AUTH_KEY', '6584D0EC602DC067346D305055041B844B6DAECBD73440B9B4B0EBC48FA635DE')

AGGREGATION_API_URL = os.environ.get(
    'AGGREGATION_API_URL',
    'https://jusilveira-aggregation-api.herokuapp.com'
)

AGGREGATION_API_AUTH_KEY = os.environ.get('AUTH_KEY', 'FADD90F58717711252A52E444278ECE5C24DD040F902C60280FDC1DD398746A8')

FIREBASE_API_KEY = os.environ.get(
    'FIREBASE_API_KEY',
    'AAAAHaZAnr8:APA91bEGMt4pvQkn6JVHFzOcSHy1exkvojaijJEy64_kCcju-eSA-hPKsWSvSjyKj5EJsTrpL_53emIvLh0aL-_IaefrRLzZA2yxC59V0DoPRDlG3Zizf0jKLwcPyZlBRUvO1-tqEMDm'
)

REDIS_URL = os.environ.get(
    'REDIS_URL',
    'redis://h:p5777486eba9f14fadf5a598c1dfa367f7bc1b51f59431977a2ae165ca81ea7e8@ec2-34-203-121-173.compute-1.amazonaws.com:7139'
)

DEBUG = os.environ.get('DEBUG', False) == 'True'
