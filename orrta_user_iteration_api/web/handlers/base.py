from orrta_user_iteration_api import settings
from tornado.web import RequestHandler
import json


def simple_auth(handler_class):

    def wrap_execute(handler_execute):
        def authorize_access(handler, kwargs):
            auth_header = handler.request.headers.get('Authorization', False)

            if not auth_header or auth_header != settings.AUTH_KEY:
                handler.set_status(401)
                handler.set_header('WWW-Authenticate', 'Basic realm=Restricted')
                handler.set_header("Content-Type", "application/json; charset=UTF-8")
                handler._transforms = []
                handler.finish()
                return False

            return True

        def _execute(self, transforms, *args, **kwargs):
            if not authorize_access(self, kwargs):
                return False

            return handler_execute(self, transforms, *args, **kwargs)

        return _execute

    handler_class._execute = wrap_execute(handler_class._execute)
    return handler_class


@simple_auth
class BaseHandler(RequestHandler):

    def bad_request(self, message):
        self.set_status(400)
        self.finish(json.dumps({
            'error': True,
            'message': message
        }))
