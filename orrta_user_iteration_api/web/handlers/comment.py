from measures import Measure
from orrta_user_iteration_api import settings
from orrta_user_iteration_api.web.handlers.base import BaseHandler
from orrta_user_iteration_api.repositories.user import UserRepository
from orrta_user_iteration_api.repositories.post import PostRepository
from orrta_user_iteration_api.repositories.comment import CommentRepository
from orrta_user_iteration_api.repositories import RepositoryException
import json
import logging

logger = logging.getLogger('CommentHandler')

measure = Measure('comment', (settings.MEASURES_URL, settings.MEASURES_PORT))


class CommentHandler(BaseHandler):

    def initialize(self):
        self.COMMENT_HEARTS_COUNT = 5

        self.user_repository = UserRepository()
        self.post_repository = PostRepository()
        self.comment_repository = CommentRepository()

    async def post(self):
        try:
            logger.info('Saving comment')

            comment = json.loads(self.request.body)
            measure.count('save', dimensions={'comment': comment})

            await self.comment_repository.create(comment.copy())
            await self.post_repository.update_comments_count(comment['postId'], comment['section'])
            await self.user_repository.update_hearts(comment['userId'], self.COMMENT_HEARTS_COUNT)
            measure.count('save-success', dimensions={'comment': comment})
        except RepositoryException as repository_error:
            measure.count('save-error', dimensions={'error': repository_error.message})
            raise repository_error

    async def delete(self):
        try:
            logger.info('Saving comment')

            comment = json.loads(self.request.body)

            measure.count('delete', dimensions={'comment': comment})

            await self.comment_repository.delete(comment['id'], comment['postId'])
            await self.post_repository.update_comments_count(
                comment['postId'],
                comment['section'],
                decrement=True
            )
            measure.count('delete-success', dimensions={'comment': comment})
        except RepositoryException as repository_error:
            measure.count('delete-error', dimensions={'error': repository_error.message})
            raise repository_error
