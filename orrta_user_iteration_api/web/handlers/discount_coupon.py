from measures import Measure
from orrta_user_iteration_api import settings
from orrta_user_iteration_api.repositories import RepositoryException
from orrta_user_iteration_api.repositories.discount_coupon import DiscountCouponRepository
from orrta_user_iteration_api.repositories.user import UserRepository
from orrta_user_iteration_api.web.handlers.base import BaseHandler
from orrta_user_iteration_api.components.fcm import FcmNotifier
import datetime
import json
import logging

logger = logging.getLogger('user-handler')

measure = Measure('discount-coupon', (settings.MEASURES_URL, settings.MEASURES_PORT))


class DiscountCouponHandler(BaseHandler):

    SENDER_USER_HEARTS_COUNT = 15
    OWNER_USER_HEARTS_COUNT = 20

    def initialize(self):
        self.discount_coupon_repository = DiscountCouponRepository()
        self.user_repository = UserRepository()
        self.fcmNotifier = FcmNotifier()

    async def post(self):
        try:
            logger.info('Saving discount coupon')

            discount_coupon = json.loads(self.request.body)

            discount_coupon['date_time'] = str(datetime.datetime.now().isoformat())

            measure.count('save', dimensions={'discount_coupon': discount_coupon})

            owner_code_user = await self.user_repository.get_by_sharing_code(discount_coupon['code'])

            if not owner_code_user:
                measure.count('save-404-error', dimensions={'discount_coupon': discount_coupon})
                return self.bad_request('Código inexistente')

            if owner_code_user['id'] == discount_coupon['user_id']:
                measure.count('save-code-ownner-error', dimensions={'discount_coupon': discount_coupon})
                return self.bad_request('Você não pode usar o próprio código')

            code_is_already_used = await self.discount_coupon_repository.code_is_already_used(
                discount_coupon['user_id'],
                discount_coupon['code']
            )

            if code_is_already_used:
                measure.count('save-already-used-error', dimensions={'discount_coupon': discount_coupon})
                return self.bad_request('Esse código já foi usado')

            await self.discount_coupon_repository.save(discount_coupon.copy())

            await self.user_repository.update_hearts(discount_coupon['user_id'], self.SENDER_USER_HEARTS_COUNT)
            await self.user_repository.update_hearts(owner_code_user['id'], self.OWNER_USER_HEARTS_COUNT)

            self.fcmNotifier.notify(
                registration_id=owner_code_user['fcmToken'],
                title='Seu cupom de desconto foi utilizado!',
                body='Você recebeu {hearts_count} corações!'.format(
                    hearts_count=self.OWNER_USER_HEARTS_COUNT
                )
            )

            measure.count('save-success', dimensions={'discount_coupon': discount_coupon})
        except RepositoryException as repository_error:
            measure.count('save-error', dimensions={'error': repository_error.message})
            raise repository_error


class DiscountCouponHandlerV2(DiscountCouponHandler):

    SENDER_USER_HEARTS_COUNT = 50
    OWNER_USER_HEARTS_COUNT = 50
