from measures import Measure
from orrta_user_iteration_api import settings
from orrta_user_iteration_api.web.handlers.base import BaseHandler
from orrta_user_iteration_api.repositories.user import UserRepository
from orrta_user_iteration_api.repositories.post import PostRepository
from orrta_user_iteration_api.repositories.hearts_sending import HeartsSendingRepository
from orrta_user_iteration_api.repositories import RepositoryException
import json
import logging

logger = logging.getLogger('HeartsHandler')

measure = Measure('hearts-sending', (settings.MEASURES_URL, settings.MEASURES_PORT))


class HeartsHandler(BaseHandler):

    def initialize(self):
        self.user_repository = UserRepository()
        self.post_repository = PostRepository()
        self.hearts_sending_repository = HeartsSendingRepository()

    async def post(self):
        try:
            logger.info('Saving hearts sending')

            hearts_sending = json.loads(self.request.body)

            measure.count('save', dimensions={'hearts_sending': hearts_sending})

            await self.hearts_sending_repository.save(hearts_sending.copy())
            await self.user_repository.update_hearts(hearts_sending['user_id'], hearts_sending['count'], True)
            await self.post_repository.update_hearts(hearts_sending['post_id'], hearts_sending['section'], hearts_sending['count'])
            measure.count('save-success', dimensions={'hearts_sending': hearts_sending})
        except RepositoryException as repository_error:
            measure.count('save-error', dimensions={'error': repository_error.message})
            raise repository_error
