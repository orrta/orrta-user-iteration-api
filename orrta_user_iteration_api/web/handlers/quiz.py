from measures import Measure
from orrta_user_iteration_api import settings
from orrta_user_iteration_api.web.handlers.base import BaseHandler
from orrta_user_iteration_api.repositories.quiz_answer import QuizAwnserRepository
from orrta_user_iteration_api.repositories.user import UserRepository
from orrta_user_iteration_api.repositories import RepositoryException
import json
import logging

logger = logging.getLogger('QuizHandler')

measure = Measure('quiz', (settings.MEASURES_URL, settings.MEASURES_PORT))


class QuizHandler(BaseHandler):

    def initialize(self):
        self.QUIZ_ANSWER_HEARTS_COUNT = 5

        self.quiz_repository = QuizAwnserRepository()
        self.user_repository = UserRepository()

    async def post(self):
        try:
            logger.info('Saving quiz answer')

            quiz_answer = json.loads(self.request.body)
            measure.count('save', dimensions={'quiz_answer': quiz_answer})

            await self.quiz_repository.save(quiz_answer.copy())
            await self.user_repository.update_hearts(quiz_answer['userId'], self.QUIZ_ANSWER_HEARTS_COUNT)
            measure.count('save-success', dimensions={'quiz_answer': quiz_answer})
        except RepositoryException as repository_error:
            measure.count('save-error', dimensions={'error': repository_error.message})
            raise repository_error
