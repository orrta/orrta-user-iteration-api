from measures import Measure
from orrta_user_iteration_api import settings
from orrta_user_iteration_api.repositories import RepositoryException
from orrta_user_iteration_api.repositories.subscription import SubscriptionRepository
from orrta_user_iteration_api.repositories.user import UserRepository
from orrta_user_iteration_api.web.handlers.base import BaseHandler
import json
import logging

logger = logging.getLogger('SubscriptionHandler')

measure = Measure('subscription', (settings.MEASURES_URL, settings.MEASURES_PORT))


class SubscriptionHandler(BaseHandler):

    def initialize(self):
        self.user_repository = UserRepository()
        self.subscription_repository = SubscriptionRepository()

    async def post(self):
        try:
            subscription = json.loads(self.request.body)

            measure.count('save', dimensions={'subscription': subscription})

            await self.user_repository.set_as_subscriber(subscription['user_id'], subscription)
            await self.subscription_repository.save_transaction(subscription)

            measure.count('save-success', dimensions={'subscription': subscription})

        except RepositoryException as repository_error:
            measure.count('save-error', dimensions={'error': repository_error.message})
            raise repository_error
        except Exception as e:
            logger.exception('Failed to create subscription')
            measure.count('save-error', dimensions={'error': e.message})
            raise e
