from measures import Measure
from orrta_user_iteration_api import settings
from orrta_user_iteration_api.web.handlers.base import BaseHandler
from orrta_user_iteration_api.repositories.user import UserRepository
from orrta_user_iteration_api.repositories import RepositoryException
import json
import logging

logger = logging.getLogger('user-handler')

measure = Measure('user', (settings.MEASURES_URL, settings.MEASURES_PORT))


class UserHandler(BaseHandler):

    def initialize(self):
        self.user_repository = UserRepository()

    async def post(self):
        try:
            logger.info('Saving user')

            user = json.loads(self.request.body)
            measure.count('save', dimensions={'user': user})

            await self.user_repository.create_or_update(user.copy())

            measure.count('save-success', dimensions={'user': user})
        except RepositoryException as repository_error:
            measure.count('save-error', dimensions={'error': repository_error.message})
            raise repository_error
