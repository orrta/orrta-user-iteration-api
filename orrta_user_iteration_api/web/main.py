from orrta_user_iteration_api import __version__ as version
from orrta_user_iteration_api import settings
from orrta_user_iteration_api.web.handlers.comment import CommentHandler
from orrta_user_iteration_api.web.handlers.discount_coupon import DiscountCouponHandler, DiscountCouponHandlerV2
from orrta_user_iteration_api.web.handlers.hearts import HeartsHandler
from orrta_user_iteration_api.web.handlers.quiz import QuizHandler
from orrta_user_iteration_api.web.handlers.subscription import SubscriptionHandler
from orrta_user_iteration_api.web.handlers.user import UserHandler
from tornado.web import Application as TornadoApplication, RequestHandler, URLSpec
import logging
import newrelic.agent
import os

logger = logging.getLogger('web')

if os.environ.get('NEW_RELIC_ENABLED', "False") is True:
    newrelic.agent.initialize('newrelic.ini')


class MainHandler(RequestHandler):
    async def get(self):
        self.write("orrta_user_iteration_api - {version}".format(version=version))


class Application(TornadoApplication):

    def __init__(self, debug=False):
        logger.info('Initializing orrta_user_iteration_api')
        super().__init__(self._routes(), debug=debug)

    def _routes(self):
        routes = (
            [
                URLSpec(r'/?', MainHandler),
                URLSpec(r'/user/?', UserHandler),
                URLSpec(r'/send-hearts/?', HeartsHandler),
                URLSpec(r'/comment/?', CommentHandler),
                URLSpec(r'/discount-coupon/?', DiscountCouponHandler),
                URLSpec(r'/discount-coupon/v2/?', DiscountCouponHandlerV2),
                URLSpec(r'/quiz-answer/?', QuizHandler),
                URLSpec(r'/subscription/?', SubscriptionHandler)
            ]
        )
        return routes


application = Application(debug=settings.DEBUG)
