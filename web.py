import os

from tornado.ioloop import IOLoop

from orrta_user_iteration_api.web.main import application

if __name__ == "__main__":
    application.listen(os.environ['PORT'])
    IOLoop.current().start()
